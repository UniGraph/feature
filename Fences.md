Be able to Organzie Collections of Graph nodes by drawing a Perimeter around them! This makes them be presented as a unit, but clicking them will expand!

- Inspired: Stardock Fences!    https://www.stardock.com/products/fences/
